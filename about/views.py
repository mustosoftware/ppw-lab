from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.
def index(request):
	response = {};
	return render(request, 'about.html', response)

def page(request, page = 'about'):
	response = {};
	filePage = {'/': 'about.html', 
				'edu': 'edu.html', 
				'signup': 'signup.html'}
	return render(request, filePage[page], response)